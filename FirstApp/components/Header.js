import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
export default function Header() {
    return (
        <View style={styles.header}>
            <Text style={styles.title}>My To-Do</Text>
        </View>
    )

}
const styles = StyleSheet.create({
    header: {
        height: 80,
        paddingTop: 38,
        backgroundColor: 'coral'
    },
    title: {
        textAlign: 'left',
        color: '#fff',
    }
});